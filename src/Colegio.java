public class Colegio {
    private static Colegio instanciaColegio; //Se crea una variable del mismo tipo de la clase
    private String nombreColegio;

    private Colegio() {//Se privatiza el constructor para que no se realice una instancia nueva en otro lugar

    }

    public static Colegio getInstancia(){
        if (instanciaColegio==null){
            instanciaColegio=new Colegio();
        }
        return instanciaColegio;
    }

    public String getNombreColegio(){
        return nombreColegio;
    }

    public void setNombreColegio(String nombreColegio){
        this.nombreColegio=nombreColegio;
    }
}
